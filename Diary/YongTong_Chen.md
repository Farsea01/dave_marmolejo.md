This is a diary by YongTong Chen E14043179

# 180920Week2 

1. Harmony view and conflict view could be used while communicating with others and finding out what conflict behavior that other people take may help you negotiate.
2. Steven Pinker states that the world is getting better.By proving that, he brought up lots of data but we thought if the world is getting better, why do people don't get as much happiness as progress of the society.

# 180927Week3

1. Should always be aware of the copyright or will get sued by the copyrigt holder.
2. Skeptical about numbers, do research yourself by checking other websites or resources.
3. Fake news happened all the time due to manipulation of politics and econimies. It blew my mind that wars can be provoked that way.

# 181004Week4

1. There are 2 approaches to justify a point, for one is hypothesis-based, for the other is find resources to generate hypothesis.
2. It is not the best way to prove a point using same resources.
3. Banks create money. If a central bank holds 95% deposit, it is easy to get corrupted and market won't work the way it should've.

# 181011Week5

1. There are different types of banks. Central banks print money while commercial banks do create money(numbers) in the account which most of people deposit their money in and transfer money from and into. Central banks can only print out the money based on the gold reserve. How much money can a commercial bank create? What are their relations with the trust, belief in the society?
2. MPC=0.5 contributes to the final total spending of 2. Small amount of increase in MPC can make great impact to the final total spending in the market.
3. Be aware of ideology of those who control data and have information. With AI technology applied to data collecting and biotech, the technologies could become stronger tools manipulating politics than ever.

# 181018Week6

1. "Well-justified belief" is knowledge.
2. Pay attention to selection bias when given information.
3. A single workout shortens reaction time.
4. Change sick care to health care to really benefit human beings.

#181025Week7

1. Exercise helps to generate dopamine in human bodies and it can lead to good mood.
2. By just listening to suicidal people can be great help to prvent suicide committed.
3. The suicide rate among black children has doubled in the past 20 years.
4. Although the designer of the GGB said the bridge is sucide-proof, somehow the romantic charisma has attracted people to jump over the bridge.

#181101Week8

1. Be a good listener. Ask them what can I help you or simply be their company when your friend is in depression.
2. When structuring a presentation, we'd better had it done with good logic starting from the meaning of the claim, then finding sources to prove it. This way we can really put things into perspective.
3. Violence is nurtured not natural in human beings.
4. Self-acknowledgement is actually the self-interpretation.

#181108Week9

1. legal fiction is a fact created by courts in order to restrict certain behavior of people and we're sent into that system once we're born when signed for the paper work.
2. money functions as currency among transactions and it is created by government making the whole society believe it has value.
3. Actually it is your signature creating the money.

#181122Week11

1. Looking back into the monetary system in history helps us to realize the monetary system share some characteristic alike with today's ecomics.
2. In Rome, Diocletian meddled with the economy by setting up laws for people to follow. The result turned out badly.
3. The causes of disappearance of sea people combine lots of factors altogether. It is quite chaotic not as simple as a linear process.

#181129Week12

1. GDP is an index invented in 1930s and the growth of GDP is not the solution for country growth also for earth.
2. Limitation motivates creativity. People usually make something useful and fantastic in situation having boundaries.
3. Lots of things are putting earth to its limit, such as human growth and we can see that from growth of different parameters in past fifty years.

#181206Week13
1. For novel entities boundary, it's hard to quantified because of too many different chemicals being concerned. Professor pointed out the fact that right now there is no restriction on the production of any chemicals. To address the "novel entities" boundary, this should be a good start.
2. When we look at the planetary boundaries, we should look at it as a whole because one interact with another. However, it is easier to handle the problems one by one.

#181213Week14
1. Only having a stable income is relatively poor in the society.
2. Central bank is owned by the government which consist of different individuals. In other words, it is still manipulated by people.
3. The policy of basic income is still an experiment. We can't know whether it's good when put into practice.

#181227Week16
1. The first video breaks down some reasons for making things go viral also talking about how to gather people around to make great achievements.
2. Education should enable students to express their ideas and have a real conversation with each other and also to the society.
3. Our group should redo the investigaton and focus on the changes of rent, customers, income around the whole area instead of focusing on one specific shop.
4. Block chain helps us to lower entry level and may solve inequity in the future.

#190103Week17
1. Playing up your own advantage because those who love you will love you still. Those who don't like you at first may change their mind when they see your differnece from others.
2. The math shows that we should reject the first 37% of people in our dating phase in order to meet our perfect partners.
3. The couples talking about, arguing over trivial and bothering things in their relationship tned to last longer.
4. Online dating apps are tools we can use to broaden the pools of people so that we can meet people that we don't see in real life.

#190107
1. successful and productive
2. Do the job for the lab 
3. Wake up and have breakfast with friends which is a good start of the day
4. Prepare for final exams.
#190108
1. Not successful and productive
2. Wake up late so everything is behind the schedule
3. Do a quick interview with the boss of London Pie and have a pleasant chat
4. Stay up late to make up for what I should've done earlier