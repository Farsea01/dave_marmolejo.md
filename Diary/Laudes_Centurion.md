# 2019-09-19

1. Today's class was really good, it kept me motivated the whole time.
2. The professor explained economy-related subjects that make me feel interesting about how the economy works in the world and its study field.
3. I appreciate that today's video had Chinese subtitles, it's hard to take classes in a second language and catch everything up.
4. I feel so good working with my team because they are inspired while working on the mini-problem cases and today we presented our work. 
5. The first time of the class, where the professor talked about "How to solve conflicts", got everyone's attention.

# 2019-09-26

1. Today we had the opportunity to listen to our classmates' team works about Steven Pinker's claims on his "Is the world getting better or worse?" Ted Talk.  
2. The Teacher gave some recommendations about how to improve our presentations that were useful for everybody.
3. We learned some practical tools to differentiate real news from fake ones. 

# 2019-10-03

1. My group had the chance to present our mini case.
2. We lost one group member (dropped the course).
3. I got confused with the economical terms but I am glad that I got the chance to learn from it.
4. I review the class at home (searched deeper) and got to the conclusion that my country is passing through a recession. I woke up the next morning with the news all over the internet.
5. This class challenges me to think beyond my biomedical or engineering fields and show me how to see the world with different eyes.
6. I am glad that we have the chance to work in groups because that gives us the chance to know better our classmates, their point of view and we can see the projects that they put their effort onto. 

# 2019-10-17

1. I learned how to make a ppt properly.
2. The differences between nationalism and fascism, and how they can be accidentally related.
3. The importance of forgiveness.
4. We all have/had potholes. Potholes are abuses, traumas, untreated mental ills, etc...
5. The importance of not judging anyone before knowing them.
6. Empathy, kindness, and compassion are not only necessary for us, individually, but it is also important to us, collectively, as a nation.
7. The teacher spoke about Sweeden (and some countries related to) history and how the anti-immigration ideas and way of actions are gaining more and more popularity nowadays. 
8. I learned how strong is the racism in other countries. I have never had a racism experience before and I never imagined it could be this way.
9. Countries have to be compassionate, kind, smart and brave to succeed on long terms.
10. Small or absurds differences make people fight against each other.
11. Small actions go a long long way <3

# 2019-10-24

1. The class has changed its format. Today we worked in supergroups, a mix of three groups.
2. We learned how to make a good hypothesis. The whole class still needs to improve it. 
3. A lot of NCKU's students are going through a mental disease.
4. Made a quick experiment which shows how our emotions and bodies are related.
5. Today's class was about mental health.
6. I learned more about the importance of regular exercise.
7. Prevent the disease can save us a lot of money.
8. Importance of our mental health.

# 2019-10-31
1. Depression is the first cause of disability nowadays.
2. A friend can suffer from depression and we can not notice it.
3. Depressed people can be cured with professional help.
4. Do not push the depressed people to do something, give them their time.
5. Listen, listen, listen to depressed people, they need to share their story.


# 2019-11-07
1. People can change their first thoughts if you make them think twice.
2. Thoughts are easy to manipulate. 
3. People can put ideas on other people's minds.
4. Happiness is a path, a journey.
5. Happiness can be only achieved by living a purposed life.
6. Success is only defined by you, and it is not applicable to other's life.

# 2019-11-14
1. The classmates presented rules to be successful and happy in life.
2. In groups, we discussed external and internal things that we want to change in our lives, and, how to change them.
3. We learned about the UK's common law.
4. We do not have to give our information to police officers if we are not doing anything against the law.
5. Our house is our castle and nobody has the right to get in without our permission.
6. Freedom is a state of mind. We are the only ones who put ourselves in a cage.

# 2019-11-21
1. The class was about the importance of social media.
2. The technology has its advantages and disadvantages.
3. Our generation is used to get what we want in familiar circles (parents, school) but gets frustrated when facing new areas (university, work).
4. Algorithms are important but they are based on the creator's desire.

# 2019-11-28
1. The class was about law.
2. Explore other civilizations, as the roman. 
3. It took in account the economy of the romans as the main factor for its decadency.
4. All the money goes back to USA.
5. Looked into the different perspectives of some countries' papernews.

# 2019-12-05
1. Shared the group prject about the papernews, it was a long research but made me understand the importance of knowing who is writing what.
2. Not everything that is wrtten on newspapers are 100% true.
3. I learned about the planetary bounds and the social fundations.


# 2019-12-12
1. Each group presented a planetary boundary or a social fundation.
2. We had a panel and the class could make questions to gain a greater understanding about each topic.
3. Every group made a catchy sentence defending its topic.
4. Each group suggested an easy method to aproach us to the equlibrium of these planetary boundaries and social fundations.
5. We voted for the best method three times. 

# 2019-12-19
1. We knew who project won last week's votation.
2. Each group answered a question abound the Third Revolution.
3. I understand better the Third Revolution and its importance.
4. We were divided in three groups; capitalist, workers and ambientalists.
5. The three groups debate about equality.

# 2019-12-26
1. Everybody said their 5 success rules, I will apply some, especially the one to write a very short diary every day to have your own feedback.
2. We said "thank you" to each other. It felt nice.
3. We listened to every group's presentation.

# 2019-12-26 Thursday
Unsuccessful but productive.
I felt unsuccessful because I lost a job, I felt productive because I studied efficiently.
I started using the Pomodoro technique.

# 2019-12-27 Friday
Unsuccessful and unproductive.
I am sick, and did not do so much because of it.

# 2019-12-28 Saturday
Succesful and productive.
The pomodoro technique worked and it forced me to start studying.
I studied for a few hours. I am still feeling sick.
I spent value time with a friend.

# 2019-12-29 Sunday
Succesful and productive.
Studied, started to feel better.
I had sleeping problem, last night.

# 2019-12-30 Monday
Succesful and productive.
Studied.
Still have sleeping problems.

# 2019-12-31 Tuesday
Unproductive.
Finished a class. 
Studied but felt tired and had a stomache with headache.
Spent the night with Sofi and Cris.
I selpt last night, but with problems.

# 2020-01-01 Wednesday
Productive.
Slept good.
Studied and did homework.
Homesick.
Headached.
Slept good.


# 2020-01-02
1. Every group presented their ideas.
2. Professor gave some recomendations about the ideas.
3. We voted on what ideas to implement.
4. The groups with the winning ideas gainen new members from groups whose ideas lost.

# 2020-01-09
1. Every group presented their final projects.
2. The professor gave some recomendations about the final projects.
3. We had our final exam.
4. We finished the class.  
4. 



