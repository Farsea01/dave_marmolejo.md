# 2019-09-26
* Fake news is a very controversial issue nowadays.
* Although I agree that it’s a serious problem that should be dealt with as soon as possible
* I don’t think the government should intervene with the practice of free speech 
* I don’t think the government regulation/interference will help in any ways of cultivating the truth for the people;
* there isn’t just one moral standard that can simply satisfy all people.
* Therefore, I suggest that we should set aside this “easy way” of solving the problem
* instead, invest more in educating the people with #reserach#copyright#citattion, etc.

# 2019-10-03
* Couldn't really pay attention to the video due to its British accent and inaccurate subtitles, 
* But! I have a feeling that it was something about the decentralization of the bank 
* as the solution to whatever economic problems we are facing 
* such an economic inequality
* short and long term cycles
* credits and money are not the same
* Credits are the reason why we have short and long term cycles(?)
* Remember three things
* Do best to increase productivity
* Don't have debt rise faster than income
* Don't have income rise faster than productivity
* "money is largely an illusion but has very, very real consequences"
* this quote pretty much sums up the video
    
# 2019-10-17
* Evil doesn't look obvious
* it takes a form of temptation
* Dictatorship took place in many forms throughout the history
* lead-market-oil---- and now data
* we need to focus where the control of the data is happening
* need to be more aware of my data distribution
* know my weakness
* do not have my weakness in the hands of the enemy

# 2019-10-24
* Regular exercise can make you happy
* however, it is the fundamental solution for the depression
* stress causes depression, therefore, getting rid of stress would be the fundamental solution to it.
* stress is inevitable in today's life
* so,a bit of exercise is justified.
* exercise may not be the most influential factor that makes up smarter people.
* Thinking back in my high school years, atheletes were not the most smartest students in school
* but those in choire and chess club et caetra were pretty smart
* therefore, the actual time invested in study would actually make people smarter

# 2019-10-31
* Depression isn't just about being sad.
* Loss of interest or pleasure; trouble sleeping; loss of energy; difficulty thinking
* it's little more serious than some might think
* therefore it needs appropriate attention and treatment.

# 2019-11-07
* obsession with happiness wont bring us happiness
* however, lack of will or effort towards happiness wont bring us happiness either
* therefore, we need to balance those two somehow in our lives
* vague and ambiguous, but it's not wrong either. 
* life consists of four pillars: belonging, purpose, stepping beyond yourself, and storytelling
* Obsession with happiness wont bring us happiness
* however, lack of will towards happiness wont bring us happiness either
* balance, therefore,is what we might be searching for.
* this expression may be vague and ambiguous, but it doesn't neccesarily mean that it's wrong 

# 2019-11-14
* power can't be trusted especially on other's hand.
* we must always check & doubt the we see
* and we need power and knowledge to do that
* the video we saw today reminded me of the concept panopticon
* perhaps, we may need strong regulations or restriction to keep order in our society
* but this restriction don't neccessarily have to last forever
* countries like Japan, Korea, and Taiwan that do not have strong police force like USA
* But they still maintain strong order. 
* perhaps influenced by the strong government they once had
* The terminology used in the juridical system.

# 2019-11-21
* The lecture today reminded me of the smartphone regulation that has recently passed in France
* Smartphone is indeed addctive, but I still wonder if it's neccessary to pass a law regarding the use it. 
* In fact, anything could be addictive in our lives, and I don't get why people categorize things that are okay to be addictive and things that are not

# 2019-11-28
* Tips given by the 1st video was helpfull
* Follow the 1st source like reporters
* Have patience before judging
* Compare and contrast multiple sources
* And yet, I know this is hardly practical
* Limited time, or in other words, good old lazyness 
* So, at least, I'm ganna have to know my place in the awareness
* and be humble about the areas that are not my specialty or interest 

# 2019-12-05
* The content of today's lecture focused on the liberal side of the view on the climate change / environment / GDP 
* the  argument was persuasive; however, it also raised question regarding fairness. 
* If the advanced countries built their economy based on the sacrifice of the environment, 
* so why can't the developing countries nowdays do the same?
* A countermeasure that can accepted by these developing nations ia needed. 
* Besides, the insight of the speaker in the 2nd video was also interesting
* the video reminded me of the lecture we had previously about GDP
* that GDP does not represent the well-being of the people

# 2019-12-12
* To be honest, I think there were too many planetary boundaries for me to understand all those twelve boundaries.  
* However, the discussion today helped me understand at lest some portion of each of twelve.
* I also learned that all these boundaries are closely related to each other
* and perhaps, solving all these problems isn't as complicated it seem to be. 
* Plus, I like the dynamic mood or the vive we are having recently due to all these debates and discssions
Dec 19, 2019
A.Successful / Unproductive
B.I was successful since my girlfriend and I had a decent conversation without a quarrel. However, I was unproductive, because I did not work on my assignments after the class at 5 (with exception to this diary)
C.Organizing the “To Do List” will be a good start. 

# 2019-12-20
* A.Unsuccessful / Unproductive
* B.I was unsuccessful, because I forgot to organize the goals and assignments of this week. I was unproductive, because I basically spent the whole day after school in my bed watching YouTube. 
* C.I lack motivation and dedication in overall, so I must find a way to motivate myself throughout the week. Plus, I must set a goal of this or next week to start with.

# 2019-12-21
* A.Unsuccessful / Productive
* B.Although I failed to find a way to motivate myself, I was still able to work on my assignments regarding school. 
* C.I should keep working on my motivation method. 

# 2019-12-22
* A.Successful / Productive 
* B.I finally got my “To Do List” organized, and I finished one of the papers that I have to write for the final exam. 
* C.I might as well try the motivation method suggested by the video we watched last Thursday. 

# 2019-12-23
* A.Successful / Productive
* B.I had successful oral exam in French class, and “the walking” turned out to be successful as a motivation method. I was also productive, because I got my room organized today.
* C.Keep up the work

# 2019-12-24
* A.Successful / Unproductive
* B.I haven’t got much done today; and yet, I felt successful regardless. It was Christmas Eve, and I think my job was to enjoy it.
* C.Keep up the work.

# 2019-12-25
* A.Successful / Unproductive
* B.Although I wasn’t able to enjoy Christmas with my family, I still called my family to have some bonding with them. I did what I have to do in Christmas, and therefore I was successful. However, I wasn’t productive, because I did not get any work done regarding school.
* C. 
1. Just walk outside when you are lost in your assignments  
2. Organize the room to condition yourself before the assignment
3. work outside the room you spend time to rest
4. lower the standard, and focus on the sustainability instead
5. Do not eat or drink too much. Prevent any distractions.